Local Web Server on Windows
=======

Table of Contents
---------
1. What is the Local Web Server?                                                                   
2. Prerequisites                                                                   
3. Installation                                                                   
4. Troubleshooting                                                                   
5. Helpful Links
6. Web Server Featured                                                                   

1.) What is the Local Web Server?
---------
I Setup this Local Web Server for all my project in Web and Application Developing.
I use this for my own because Im tired of using XAMP/WAMP or any 3rd Party Local Server because when I deployed it on Hosting,VPS 
I will get some error's that I dont see in Xamp/Wamp so I decide to setup my own Local Server by manual Configuration of Appache and MySql DB. 
this is good for your training because you can learn how to manage your own local server.
the good thing in having a local environment web server is you dont need to open the application to start your apache/mysql again, 
because when you open your Computer your local server will automatically start.

2.) Prerequisites
---------
Before installing there are certain tools and applications you will need.
This differs between the varying operating systems available, so the following
is broken down into Windows and Linux prerequisites.

* Windows                                                                   
        * Maria DB (mariadb-10.3.14-winx64.msi)  https://downloads.mariadb.org/mariadb/10.3.14/                                                                   
        * MySql Workbench (Latest Version) https://dev.mysql.com/downloads/workbench/                                                                   
        * Composer (Latest Version) https://getcomposer.org/                                                                   
        * Visual Studio Code (Latest Version) https://code.visualstudio.com/                                                                   
3.) Installation
---------
This section is a very brief set of installation instructions. For more concise guides
relevant to your Operation System.
* Windows                                                                                                                    
        [Step by Step Installation.]                                                                   
        * Paste the Folder (Apache24 | php | www) on your Drive C.                                                                   
        * Install Maria DB first and Setup your Username and Password for your local Database.
          Why you need to setup a password? because there is a application or api that need to communicate with the root and password privilege.                                                                                                                                      
        * Install Workbench and create an instance to connect to your MySQL Server.

4.) Troubleshooting
---------
To Start the Apache Server using Your Command Promt.
* CMD                                                                                                                    
        [Step by Step Troubleshoot]                                                                   
        * Open your CMD (Make sure to use Run As Administration) and type the commands bellow.                                                                   
        * cd \Apache24\bin (to locate the folder where your Apache file is located)                                                                   
        * httpd -k install (to install the apache server on your windows)                                                                   
        * httpd -k start (to start your apache server once. when you open your computer your                                                                    
          apache server will automaticaly running on your windows.)                                                                   
        
        [Troubleshoot]                                                                   
        * httpd help (To check what commands available for you)                                                                   
        * httpd restart (Use this command when you got error on your files if you have internal update in your Apache File)                                                                   
         
        [Creating Directory URL]                                                                   
        this is your Workspace, you can setup multiple project on Web Developing by Adding your Custom Domain for your project.
        * Open your www Folder and create your folder: for example: wedevsolution.com.ph (Make sure your custom domain is not available in WHOIS.COM)
        * Open host in your windows located in "C:\Windows\System32\drivers\etc" edit the file name "host" and put your domain like
          Example: 127.0.0.1     wedevsolution.com.ph
        * Create your virtual host in your Apache Server: Located on your "Apache24/conf/" and open the httpd-vhosts.conf and make your Virtual Host
          Example:
            <VirtualHost *:80> 
                DocumentRoot "c:/www/wedevsolution.com.ph" 
                ServerName wedevsolution.com.ph 
            </VirtualHost>
        * Open your browser and type your custom domain
            **type on browser: wedevsolution.com.ph** (for your website)
            **type on browser: phpmyadmin.com.ph** (for your phpmyadmin database dont forget to add this on your host)

        GOOD LUCK ON YOUR WORKSPACE HAPPY WEB DEVELOPING.
        
5.) Helpful Links
---------
* LINKS                                                                                                                    
        * https://downloads.mariadb.org/mariadb/10.3.14/                                                          
        * https://dev.mysql.com/downloads/workbench/                                                          
        * https://getcomposer.org/                                                          
        * https://code.visualstudio.com/                                                          
        * https://git-scm.com/

6.) Web Server Featured
---------                                                          
* Featured
        * Modules
        * .htaccess
        * proxy support
        * connection public static IP (Support Soon)